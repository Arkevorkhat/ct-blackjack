#ifndef STRUCTURES_H
#define STRUCTURES_H
#include <stdbool.h>
typedef struct CARD {
	COLOR C; //functional bool, holds either RED or BLACK
	TYPE F;
	char name[25];
	int val;
}card;
typedef struct HAND {
	card hold[12];
	int bet;
	int val;
}hand;
typedef struct DECK {
	card cards[52];
	int top; //starts at 51, goes to 0, holds index.
}deck;
typedef struct PLAYER {
	hand h;
	long int money;
};
typedef struct DEALER {
	hand house;
	deck shoe;
}dealer;
void swap(card *a, card *b) {
	card c;
	c = *a; *a = *b; *b = c;
}
typedef enum COLOR {RED, BLACK};
typedef enum TYPE {ACE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING};
typedef enum SUIT {CLUBS, HEARTS, SPADES, DIAMONDS};
#endif // !STRUCTURES_H
